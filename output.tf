output "endpoint_url" {
  value = aws_apigatewayv2_api.this.api_endpoint
}

output "id" {
  value       = aws_apigatewayv2_api.this.id
  description = "AWS API gateway's ID"
}
