terraform {
  experiments = [module_variable_optional_attrs]
}

variable "environment" {
  description = "One of `dev`, `staging` or `production`. Note that this is specified via `ops.yml` environment configuration."
  type        = string
  validation {
    condition     = index(["dev", "staging", "production"], var.environment) >= 0
    error_message = "Specify a valid environment."
  }
}

variable "namespace" {
  description = "Short name with letters, numerals and dashes. Defaults to empty but do specify if using `dev` environment."
  default     = ""
}

variable "api_name" {
  description = "Short name with letters, numerals and dashes."
}

variable "api_desc" {
  description = "The description of the API under which the proxy is deployed."
}


variable "cors" {
  description = "Provide the CORS configuration. Default to * and no credentials."
  type = object({
    allow_origins     = list(string),
    max_age           = optional(number),
    allow_credentials = bool,
    allow_methods     = optional(list(string)),
    allow_headers     = optional(list(string)),
    expose_headers    = optional(list(string))
  })
  default = {
    allow_origins     = ["*"]
    allow_credentials = false
  }
}

variable "tags" {
  description = "Map of tags to attach to any resources in this module that support tags. Defaults to `{}`."
  type        = map(string)
  default     = {}
}

variable "logging" {
  type = object({ enable = bool, retention_in_days = optional(number) })
  default = {
    enable            = true
    retention_in_days = 3
  }
  description = "API proxy logging to a CloudWatch group is enabled with 3-day retention by default."
}

variable "staged_integration" {
  type        = map(object({ integration_base_uri = string }))
  description = "Specify one or more stages, each with an integration base URI. E.g. { \"v1\" = { integration_base_uri = URI } } will create a stage named \"v1\" that will route to the specified URI."
  #
  # {
  #  "$default" = { integration_base_uri = "api0.x.com" }
  #  "v1" = { integration_base_uri = "api1.x.com" }
  # }
  #
}

variable "opt_integ_no_ssl" {
  default     = false
  type        = bool
  description = "Set to true to indicate all proxy integrations should use \"http://\" instead of \"https://\" by default."
}

variable "api_domain" {
  type        = object({ zone_name = optional(string), api_name_prefix = optional(string) })
  description = "Specify the zone's domain name and the API's domain name prefix so that the API gets a custom domain name endpoint. The zone domain must already be available as a Route53 hosted zone. If the API endpoint is the same as zone name, don't specify it or use null."
  default     = {}
}

