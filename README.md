# FaaSter/AWS/Easy HTTP Proxy

This is a Terraform module that can be used to easily define and create/deploy a CORS-enabled HTTP proxy API using API Gateway v2.

> Module works; documentation is still a work in progress.

- [Changelog](#changelog)
- [Using the Module](#using-the-module)
- [Module Reference](#module-reference)
  - [Requirements](#requirements)
  - [Providers](#providers)
  - [Modules](#modules)
  - [Resources](#resources)
  - [Inputs](#inputs)
  - [Outputs](#outputs)

## Changelog

See the [repository tags list](https://gitlab.com/faaster/aws/easy-rest-api/-/tags) for the changelog.

## Using the Module

Here's a simple deloyable example of using the module.

```hcl
module "my_easy_proxy" {
  source = "git::https://gitlab.com/faaster/aws/easy-http-proxy.git"
  environment = var.environment         # dev, staging or production
  namespace   = var.namespace           # optional, use in "dev" env
  api_name    = "my_easy_proxy"
  api_desc                   = "Sample HTTP proxy using API Gateway v2"
  proxy_integration_base_uri = "https://qrng.anu.edu.au/API/jsonI.php"
}
```

## Module Reference

### Requirements

| Name                                                                      | Version   |
| ------------------------------------------------------------------------- | --------- |
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 0.14.0 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws)                   | >= 3.30.0 |

### Providers

| Name                                                                      | Version   |
| ------------------------------------------------------------------------- | --------- |
| <a name="provider_aws"></a> [aws](#provider\_aws)                         | >= 3.30.0 |
| <a name="provider_aws.useast1"></a> [aws.useast1](#provider\_aws.useast1) | >= 3.30.0 |

### Modules

No modules.

### Resources

| Name                                                                                                                                      | Type        |
| ----------------------------------------------------------------------------------------------------------------------------------------- | ----------- |
| [aws_apigatewayv2_api.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/apigatewayv2_api)                 | resource    |
| [aws_apigatewayv2_api_mapping.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/apigatewayv2_api_mapping) | resource    |
| [aws_apigatewayv2_domain_name.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/apigatewayv2_domain_name) | resource    |
| [aws_apigatewayv2_integration.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/apigatewayv2_integration) | resource    |
| [aws_apigatewayv2_route.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/apigatewayv2_route)             | resource    |
| [aws_apigatewayv2_stage.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/apigatewayv2_stage)             | resource    |
| [aws_cloudwatch_log_group.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudwatch_log_group)         | resource    |
| [aws_route53_record.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route53_record)                     | resource    |
| [aws_acm_certificate.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/acm_certificate)                | data source |
| [aws_route53_zone.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/route53_zone)                      | data source |

### Inputs

| Name                                                                                       | Description                                                                                                                                                                                                                                                         | Type                                                                                                                                                                                                                                                                                                       | Default                                                                                      | Required |
| ------------------------------------------------------------------------------------------ | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | -------------------------------------------------------------------------------------------- | :------: |
| <a name="input_api_desc"></a> [api\_desc](#input\_api\_desc)                               | The description of the API under which the proxy is deployed.                                                                                                                                                                                                       | `any`                                                                                                                                                                                                                                                                                                      | n/a                                                                                          |   yes    |
| <a name="input_api_domain"></a> [api\_domain](#input\_api\_domain)                         | Specify the zone's domain name and the API's domain name prefix so that the API gets a custom domain name endpoint. The zone domain must already be available as a Route53 hosted zone. If the API endpoint is the same as zone name, don't specify it or use null. | `object({ zone_name = optional(string), api_name_prefix = optional(string) })`                                                                                                                                                                                                                             | `{}`                                                                                         |    no    |
| <a name="input_api_name"></a> [api\_name](#input\_api\_name)                               | Short name with letters, numerals and dashes.                                                                                                                                                                                                                       | `any`                                                                                                                                                                                                                                                                                                      | n/a                                                                                          |   yes    |
| <a name="input_cors"></a> [cors](#input\_cors)                                             | Provide the CORS configuration. Default to * and no credentials.                                                                                                                                                                                                    | <pre>object({<br>    allow_origins     = list(string),<br>    max_age           = optional(number),<br>    allow_credentials = bool,<br>    allow_methods     = optional(list(string)),<br>    allow_headers     = optional(list(string)),<br>    expose_headers    = optional(list(string))<br>  })</pre> | <pre>{<br>  "allow_credentials": false,<br>  "allow_origins": [<br>    "*"<br>  ]<br>}</pre> |    no    |
| <a name="input_environment"></a> [environment](#input\_environment)                        | One of `dev`, `staging` or `production`. Note that this is specified via `ops.yml` environment configuration.                                                                                                                                                       | `string`                                                                                                                                                                                                                                                                                                   | n/a                                                                                          |   yes    |
| <a name="input_logging"></a> [logging](#input\_logging)                                    | API proxy logging to a CloudWatch group is enabled with 3-day retention by default.                                                                                                                                                                                 | `object({ enable = bool, retention_in_days = optional(number) })`                                                                                                                                                                                                                                          | <pre>{<br>  "enable": true,<br>  "retention_in_days": 3<br>}</pre>                           |    no    |
| <a name="input_namespace"></a> [namespace](#input\_namespace)                              | Short name with letters, numerals and dashes. Defaults to empty but do specify if using `dev` environment.                                                                                                                                                          | `string`                                                                                                                                                                                                                                                                                                   | `""`                                                                                         |    no    |
| <a name="input_opt_integ_no_ssl"></a> [opt\_integ\_no\_ssl](#input\_opt\_integ\_no\_ssl)   | Set to true to indicate all proxy integrations should use "http://" instead of "https://" by default.                                                                                                                                                               | `bool`                                                                                                                                                                                                                                                                                                     | `false`                                                                                      |    no    |
| <a name="input_staged_integration"></a> [staged\_integration](#input\_staged\_integration) | Specify one or more stages, each with an integration base URI. E.g. { "v1" = { integration\_base\_uri = URI } } will create a stage named "v1" that will route to the specified URI.                                                                                | `map(object({ integration_base_uri = string }))`                                                                                                                                                                                                                                                           | n/a                                                                                          |   yes    |
| <a name="input_tags"></a> [tags](#input\_tags)                                             | Map of tags to attach to any resources in this module that support tags. Defaults to `{}`.                                                                                                                                                                          | `map(string)`                                                                                                                                                                                                                                                                                              | `{}`                                                                                         |    no    |

### Outputs

| Name                                                                       | Description                                                     |
| -------------------------------------------------------------------------- | --------------------------------------------------------------- |
| <a name="output_endpoint_url"></a> [endpoint\_url](#output\_endpoint\_url) | n/a                                                             |
| <a name="output_graphviz"></a> [graphviz](#output\_graphviz)               | Description of the module instance for use with Graphviz `dot`. |
| <a name="output_id"></a> [id](#output\_id)                                 | AWS API gateway's ID                                            |
