# -------------- domain name if requested
locals {
  # determine if domain is needed
  # a list of one or none
  want_domain = !((var.api_domain == null) || (var.api_domain.zone_name == null))
  # prepare prefix iff api_name_prefix is required. otherwise use the zone name as API name.
  deploy_dom_prefix  = local.want_domain ? ((var.api_domain.api_name_prefix == null || var.api_domain.api_name_prefix == "") ? "" : "${var.api_domain.api_name_prefix}.") : null
  deploy_domain_list = local.want_domain ? { "${local.deploy_dom_prefix}${var.api_domain.zone_name}" = var.api_domain.zone_name } : {}
}

// We need to make sure we have this cert in `us-east-1` for use with CloudFront.
provider "aws" {
  alias  = "east1cdn"
  region = "us-east-1"
}

#
# If domain name specified, then map it to the deployment stage
#
data "aws_route53_zone" "this" {
  for_each = local.deploy_domain_list
  name     = each.value
}

data "aws_acm_certificate" "this" {
  provider = aws.useast1
  for_each = local.deploy_domain_list
  domain   = each.key
  statuses = ["ISSUED"]
}

resource "aws_apigatewayv2_domain_name" "this" {
  for_each    = local.deploy_domain_list
  domain_name = data.aws_acm_certificate.this[each.key].domain

  domain_name_configuration {
    certificate_arn = data.aws_acm_certificate.this[each.key].arn
    endpoint_type   = "REGIONAL"
    security_policy = "TLS_1_2"
  }
}

resource "aws_route53_record" "this" {
  for_each = local.deploy_domain_list
  name     = aws_apigatewayv2_domain_name.this[each.key].domain_name
  type     = "A"
  zone_id  = data.aws_route53_zone.this[each.key].zone_id

  alias {
    name                   = aws_apigatewayv2_domain_name.this[each.key].domain_name_configuration[0].target_domain_name
    zone_id                = aws_apigatewayv2_domain_name.this[each.key].domain_name_configuration[0].hosted_zone_id
    evaluate_target_health = false
  }
}

resource "aws_apigatewayv2_api_mapping" "this" {
  for_each = local.want_domain == false ? {} : { for sn, sd in var.staged_integration : sn => "${local.deploy_dom_prefix}${var.api_domain.zone_name}" }
  # for_each    = local.deploy_domain_list
  api_id          = aws_apigatewayv2_api.this.id
  stage           = aws_apigatewayv2_stage.this[each.key].id
  domain_name     = aws_apigatewayv2_domain_name.this[each.value].id
  api_mapping_key = each.key == "$default" ? "" : each.key
}
