
locals {
  integ_protocol = "${var.opt_integ_no_ssl ? "http" : "https"}://"
}
resource "aws_apigatewayv2_integration" "this" {
  api_id             = aws_apigatewayv2_api.this.id
  integration_type   = "HTTP_PROXY"
  integration_method = "ANY"
  integration_uri    = "${local.integ_protocol}$${stageVariables.INTEG_URI}/{proxy}" #"${var.proxy_integration_base_uri}/{proxy}"
}

resource "aws_apigatewayv2_route" "this" {
  api_id    = aws_apigatewayv2_api.this.id
  route_key = "ANY /{proxy+}"

  target = "integrations/${aws_apigatewayv2_integration.this.id}"
}

