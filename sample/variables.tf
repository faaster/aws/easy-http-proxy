variable "aws_region" {
  description = "AWS region to use when setting up infrastructure resources that are region specific. Defaults to `us-east-1`."
  default     = "us-east-1"
}

variable "environment" {
  description = "One of `dev`, `staging` or `production`. Note that this is specified via `ops.yml` environment configuration."
  type        = string
  validation {
    condition     = index(["dev", "staging", "production"], var.environment) >= 0
    error_message = "Specify a valid environment."
  }
}

variable "namespace" {
  description = "Short name with letters, numerals and dashes. Defaults to empty but do specify if using `dev` environment."
  default     = ""
}

variable "domain_zone_name" {
  description = "Route53 domain zone name to use for custom domain prefix."
}
