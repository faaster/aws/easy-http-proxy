// Required terraform and provider versions
terraform {
  required_version = ">= 0.14.0"
}

// Define Product Web Link AWS provider
provider "aws" {
  region = var.aws_region
}

module "sample_http_proxy" {
  source      = "../"
  environment = var.environment
  namespace   = var.namespace
  api_name    = "my_easy_proxy"
  api_domain = {
    zone_name       = var.domain_zone_name
    api_name_prefix = "sample"
  }
  cors = {
    allow_origins     = ["https://app.wishslate.com"]
    allow_methods     = ["GET", "POST"]
    allow_headers     = ["content-type"]
    expose_headers    = ["content-type"]
    allow_credentials = true
  }
  api_desc = "Sample HTTP proxy using API Gateway v2"
  staged_integration = {
    "$default" = {
      integration_base_uri = "qrng.anu.edu.au/API/jsonI.php"
    }
    "jsonip" = {
      integration_base_uri = "jsonip.com"
    }
  }
}

output "api" {
  value = module.sample_http_proxy.endpoint_url
}

output "graphviz" {
  value = <<-EOT
  digraph {
    rankdir=LR
    fontname="Helvetica"; fontsize=12;
    node[fontname="Helvetica", fontsize=11]
    edge[fontname="Helvetica", fontsize=9]

    ${module.sample_http_proxy.graphviz.subgraph}
  }
  EOT
}
