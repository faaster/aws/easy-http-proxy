
locals {
  dot_idhash = md5(aws_apigatewayv2_api.this.arn)
  dot_api    = "${aws_apigatewayv2_api.this.name}_${local.dot_idhash}"

  api_1  = split("//", aws_apigatewayv2_api.this.api_endpoint)
  api_2  = split(".", local.api_1[1])
  api_ww = join("//\\n", [local.api_1[0], join("/\\n", local.api_2)])

  stage_first = "stage_${replace(keys(var.staged_integration)[0], "$", "_")}_${local.dot_idhash}"
}

#
# Domain/Cert related graphviz locals
#
locals {
  dom_key             = local.want_domain ? "${local.deploy_dom_prefix}${var.api_domain.zone_name}" : ""
  zone_name           = local.want_domain ? var.api_domain.zone_name : "-- not specified --"
  cert_name           = local.want_domain ? data.aws_acm_certificate.this[local.dom_key].domain : "-- not specified --"
  arec_name           = local.want_domain ? aws_apigatewayv2_domain_name.this[local.dom_key].domain_name : "-- not defined --"
  api_domain_subgraph = <<-EOT
    subgraph {
      node [shape=record,style=dashed ${local.want_domain ? ",fontcolor=blue" : ",color=grey,fontcolor=grey"}]
      tls_cert_${local.dot_idhash} [label = "AWS TLS Cert | ${local.cert_name}"]
    }
    subgraph cluster_r53zone_${local.dot_idhash} {
      label = "R53 Zone:\n${local.zone_name}"
      style="dashed, rounded"
      ${local.want_domain ? "color=black; fontcolor=blue;" : "color=grey; fontcolor=grey;"}
      node [shape=record,style=solid ${local.want_domain ? "" : ",color=grey,fontcolor=grey"}]
      r53_dns_arec_${local.dot_idhash} [label = "DNS A Record | ${local.arec_name}"]
    }

    ${local.want_domain ? "" : "edge[color=grey,fontcolor=grey]"}
    tls_cert_${local.dot_idhash} -> r53_dns_arec_${local.dot_idhash} [dir=back]
    ${local.stage_first} -> r53_dns_arec_${local.dot_idhash} [dir=back ltail=cluster_stages_${local.dot_idhash}]
  EOT
}

#
# Logging related graphviz locals
#
locals {
  is_logging = var.logging.enable == true
  # logg_label           = local.is_logging ? "${aws_cloudwatch_log_group.this["api"].name_prefix}*" : "-- Disabled --"
  api_logging_subgraph = <<-EOT
    subgraph {
      node [shape=record,style=solid ${local.is_logging ? "" : ",color=grey,fontcolor=grey"}]
      ${join("\n", [for sn, sd in var.staged_integration : "log_group_${replace(sn, "$", "_")}_${local.dot_idhash} [label=\"AWS CW Log Group | ${local.is_logging ? aws_cloudwatch_log_group.this[sn].name_prefix : "-- disabled --"}\"]"])}

      ${local.is_logging ? "" : "edge[color=grey,fontcolor=grey]"}
      ${join("\n", [for sn, sd in var.staged_integration : "log_group_${replace(sn, "$", "_")}_${local.dot_idhash} -> stage_${replace(sn, "$", "_")}_${local.dot_idhash} [label=\"logging\" dir=back]"])}
    }
  EOT
}

#
# The output aggregating it all together
#
output "graphviz" {
  description = "Description of the module instance for use with Graphviz `dot`."
  value = {
    subgraph = <<-EOT
      compound=true
      subgraph cluster_${local.dot_api} {
        label="API: ${aws_apigatewayv2_api.this.name}"
        style="rounded"; labelloc=t; color=grey;

        subgraph cluster_stages_${local.dot_idhash} {
          node [shape="record"]
          label = "APIv2 Stages"
          ${join("\n", [for sn, sd in var.staged_integration : "stage_${replace(sn, "$", "_")}_${local.dot_idhash} [label=\"${sn} | ${local.api_ww}/${sn == "$default" ? "" : sn}\"]"])}
        }

        subgraph {
          node [shape="box", fillcolor=lightgrey, style="dashed,filled,rounded"]
          ${join("\n", [for sn, sd in var.staged_integration : "integ_uri_${replace(sn, "$", "_")}_${local.dot_idhash} [label=\"${local.integ_protocol}\n${sd.integration_base_uri}\"]"])}
        }

        ${join("\n", [for sn, sd in var.staged_integration : "integ_uri_${replace(sn, "$", "_")}_${local.dot_idhash} -> stage_${replace(sn, "$", "_")}_${local.dot_idhash} [dir=back]"])}


        ${local.api_domain_subgraph}
        ${local.api_logging_subgraph}

      }
    EOT
    nodes = {
      this = local.dot_api
    }
  }
}
