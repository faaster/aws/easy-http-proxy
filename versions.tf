// Required terraform and provider versions
terraform {
  required_version = ">= 0.14.0"
}

terraform {
  required_providers {
    aws = {
      version = ">= 3.30.0"
      source  = "hashicorp/aws"
    }
  }
}
