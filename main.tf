
# US East 1 region for specific CDN-related use cases.
provider "aws" {
  region = "us-east-1"
  alias  = "useast1"
}

locals {
  # Determine if dev environment
  isdev = (lower(var.environment) == "dev")
  # Determine namespace prefix/suffix if needed (not for production)
  nsprefix = local.isdev ? (var.namespace != null ? "${var.namespace}_" : "") : ""
  nssuffix = local.nsprefix != "" ? "_${var.namespace}" : ""
  # Determine name prefix/suffix if needed (not for production)
  deployprefix = local.isdev ? "${var.environment}_${local.nsprefix}" : ""
  deploysuffix = local.isdev ? "_${var.environment}${local.nssuffix}" : ""
  ## Determine deployment tag
  deploylabel = local.isdev ? "${var.environment}${local.nssuffix}" : "Production"
  deployhead  = title("${local.deployprefix}deployment ")

  tags = merge(var.tags, {
    label       = local.deploylabel,
    environment = var.environment
  })
}
