
resource "aws_apigatewayv2_api" "this" {
  name          = "${local.deployprefix}${var.api_name}"
  description   = "${var.environment == "production" ? "" : local.deployhead}${var.api_desc}"
  protocol_type = "HTTP"
  tags          = local.tags

  cors_configuration {
    allow_methods     = var.cors.allow_methods
    allow_credentials = var.cors.allow_credentials
    allow_origins     = var.cors.allow_origins
    allow_headers     = var.cors.allow_headers
    expose_headers    = var.cors.expose_headers
    max_age           = var.cors.max_age
  }
}

resource "aws_cloudwatch_log_group" "this" {
  for_each          = var.logging.enable ? var.staged_integration : {}
  name_prefix       = "${local.deployprefix}${var.api_name}${each.key == "$default" ? "" : "_${each.key}"}"
  retention_in_days = var.logging.retention_in_days == null ? 3 : var.logging.retention_in_days
}

resource "aws_apigatewayv2_stage" "this" {
  for_each    = var.staged_integration
  api_id      = aws_apigatewayv2_api.this.id
  name        = each.key # "$default"
  auto_deploy = true
  stage_variables = {
    "INTEG_URI" = each.value.integration_base_uri
  }
  dynamic "access_log_settings" {
    # only if logging enabled
    for_each = toset(var.logging.enable ? [each.key] : [])
    iterator = log_each
    content {
      destination_arn = aws_cloudwatch_log_group.this[log_each.key].arn
      # https://docs.aws.amazon.com/apigateway/latest/developerguide/http-api-logging-variables.html
      format = jsonencode({
        caller         = "$context.identity.caller"
        httpMethod     = "$context.httpMethod"
        ip             = "$context.identity.sourceIp"
        protocol       = "$context.protocol"
        requestId      = "$context.requestId"
        requestTime    = "$context.requestTime"
        responseLength = "$context.responseLength"
        status         = "$context.status"
        user           = "$context.identity.user"
      })
    }
  }

  default_route_settings {
    throttling_rate_limit  = 100
    throttling_burst_limit = 25
  }

}
